const mongoose = require('mongoose');

const employeesSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
  idCompany: String,
	name: String,
	departament: String
}, {
	versionKey: false // Unable auto-version after persist database
});

const Employees = mongoose.model('employees', employeesSchema);

module.exports = Employees;
