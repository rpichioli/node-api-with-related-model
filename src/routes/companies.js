const express = require('express');
const mongoose = require('mongoose');
const Companies = require('../models/companies'); // Model

// Initiating router
const router = express.Router(); // Used to assign into a Node server route

/** Find all companies */
router.get('/', (req, res) => {
	Companies.find({}, (err, companies) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Invalid data received
		if (!companies) res.status(401).json({ error: "Unauthorized action!" });
		// Everything OK
		res.json({ success: true, companies }); // Return collection saved including the MongoDB _id
	});
})

/** Find company by identifier (id) */
router.get('/:id', (req, res) => {
	let _id = req.params.id || null;

	// Basic identifier validation
	if (!_id)
		res.status(400).json({ success: false, error: "Invalid identifier has been sent!" });

	// Converting ID to OID through mongoose
	_id = mongoose.Types.ObjectId(_id);

	// Querying by document '$oid'
	Companies.find({ _id }, (err, company) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Invalid data received
		if (!company) res.status(401).json({ error: "Unauthorized action!" });
		// Everything OK
		res.json({ success: true, company });
	});
})

/** Find companies by identifier (name) */
router.get('/by-name/:name', (req, res) => {
	let name = req.params.name || null;

	// Basic identifier validation
	if (!name) res.status(400).json({ success: false, error: "Invalid identifier has been sent!" });

	// Querying by document 'name'
	// If you like to limit for the 5 firts use: Companies.find({name: { $regex: '.*' + name + '.*' } }).limit(5);
	Companies.find({name: { $regex: '.*' + name + '.*' } }, (err, companies) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Invalid data received
		if (!companies) res.status(401).json({ error: "Unauthorized action!" });
		// Everything OK
		res.json({ success: true, companies });
	});
})

/** New company */
router.post('/', (req, res) => {
	let { name, address, email, tel, employees } = req.body;
	let _id = mongoose.Types.ObjectId(); // Generating new MongoDB _ID

	Companies.create({ _id, name, address, email, tel, employees }, (err, company) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!", err });
		// Everything OK
		res.status(201).json({ success: true, company });
	});
});

/** Update company */
router.put('/', (req, res) => {
	let { _id, name, address, email, tel, employees } = req.body;

	// Find the company by it's ID and update it
	Companies.findByIdAndUpdate(
		_id,
		{ $set: { _id, name, address, email, tel, employees } }, // spotlight
		{ new: true },
		(err, company) => {
			// Something wrong happens
			if (err) res.status(400).json({ success: false, error: "Can't update company!" });
			// Everything OK
			res.json({ success: true, company });
		}
	);
});

/** Delete company */
router.delete('/:id', (req, res) => {
	const _id = req.params.id || null;

	// Remove company by it's _ID
	if (_id) {
		Companies.deleteOne({ _id }, err => {
			// Something wrong happens
			if (err) res.status(400).json({ success: false, error: "Can't remove company!" });
			// Everything OK
			res.json({ success: true });
		});
	} else {
		res.status(400).json({ error: "Source required to perform the search!" });
	}
});

module.exports = router; // Exporting as a default router
