const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();
const port = process.env.PORT;

// Express application
const app = express();

// MongoDB (Mongoose) connection
const db = mongoose.connect(process.env.DB_CONN, { useNewUrlParser: true });
// Event interceptors if you want to check status
mongoose.connection.on('connected', () => console.log('MongoDB (Mongoose) - Connected'));
mongoose.connection.on('error', error => console.log('MongoDB (Mongoose) - Error', error));
mongoose.connection.on('disconnected', () => console.log('MongoDB (Mongoose) - Disconnected'));

// Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Middleware allowing all for testing purposes (methods, cross-origin, ..)
app.use(require('./middlewares/cors').allowAll);

// Requiring express.Router() modules to assign in routes
const api_companies = require('./routes/companies');
const api_employees = require('./routes/employees');
const api_swagger = require('./routes/swagger');

// Assigning routes to imported routers
app.use('/', api_swagger);
app.use('/companies/', api_companies);
app.use('/employees/', api_employees);

// Default error handler - Detect any error in Express and handle it in middleware
app.use((err, req, res, next) => {
	// Runtime error output in server terminal
	console.error(err);
	// Send generic error message under 500 status response
	res.status(500).json({ error: 'Something went wrong! Verify the URL you tried to access and if you are filling the parameters correctly if they are needed.' });
});

// Deny access for any route not mapped
app.use((req, res) => res.status(403).json({ error: 'Forbidden access! URL not allowed.' }));

// Starting application server listening to it's port
app.listen(port, () => console.log(`Server listening to ${port}`));
